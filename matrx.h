#include<string>

using namespace std;

#ifndef MATRXCL


class Matrx
{
private:
    int _m, _n;
    double **p;
    
public:
    Matrx(int m, int n);
    ~Matrx();
    double getItem(int i, int j);
    double setItem(int i, int j, double v);
    int Rows();
    int Cols();
    
    void readMatrx(string& s);
    void writeMatrx();
};

#endif