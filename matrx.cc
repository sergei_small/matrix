#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<fstream>
#include<string>
#include "matrx.h"

using namespace std;

Matrx::Matrx (int m, int n)
{
    _m=m;
    _n=n;
    p = new double*[_m];
    for(int i=0; i<_n; i++)
        p[i] = new double[_n];    
}

Matrx::~Matrx ()
{
    for (int i=0; i<_m; i++)
        delete[] p[i];
        
    delete[] p;
}

int Matrx::Rows()
{
    return _m;
}

int Matrx::Cols()
{
    return _n;
}

double Matrx::getItem(int i, int j)
{
    return p[i][j];
}

double Matrx::setItem(int i, int j, double v)
{
    p[i][j] = v;
    return p[i][j];
}

void Matrx::readMatrx(string& s)
{
    double temp;
    ifstream matrxFL;
    matrxFL.open(s.c_str());
    
    for (int i=0; i<_m; i++)
        for (int j=0; j<_n; j++)
        {
            matrxFL >> temp;
            setItem(i,j,temp);
        }
    
    matrxFL.close();
}

void Matrx::writeMatrx()
{
    for (int i=0; i<_m; i++)
    {
    for (int j=0; j<_n; j++)
            cout << getItem(i,j) << " ";
        cout << endl;
    }
}
